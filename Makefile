# Makefile for rk_bios_dump for MinGW-W64 + M-SYS
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
CC_PREFIX = $(PREFIX)
CC_PATH =

GCC = $(CC_PATH)$(CC_PREFIX)gcc
GPP = $(CC_PATH)$(CC_PREFIX)g++
AR  = ar
WRC = windres
FCG = fltk-config --use-images

# FLTK configs 
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# Base PATH
BASE_PATH = .
SRC_PATH  = $(BASE_PATH)/src
LOCI_PATH = /usr/local/include
LOCL_PATH = /usr/local/lib

# TARGET settings
TARGET_PKG = rkbiosdump
TARGET_DIR = ./bin/Release
TARGET_OBJ = ./obj/Release
LIBWIN32S  = -lole32 -luuid -lcomctl32 -lwsock32 -lm -lgdi32 -luser32 -lkernel32 -lShlwapi -lcomdlg32

# DEFINITIONS
DEFS  = -D_POSIX_THREADS -DSUPPORT_DRAGDROP
DEFS += -DWIN32 -D_WIN32 -DUNICODE -D_UNICODE -DSUPPORT_WCHAR 
DEFS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_THREAD_SAFE -D_REENTRANT

# Compiler optiops 
COPTS  = -m64 -mms-bitfields -mwindows 
COPTS += -ffast-math -fexceptions -O3 -s

# CC FLAGS
CFLAGS  = -I$(SRC_PATH)
CFLAGS  = -I$(SRC_PATH)/fl
CFLAGS += -I$(LOCI_PATH) -Ires
CFLAGS += $(FLTKCFG_CXX)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# Windows Resource Flags
WFLGAS  = $(CFLAGS)

# LINK FLAG
LFLAGS  =
LFLAGS += -L$(FIMG_PATH)
LFLAGS += -L$(LOCL_PATH)
LFLAGS += -static
LFLAGS += ${FLTKCFG_LFG}
LFLAGS += -lpthread
LFLAGS += $(LIBWIN32S)

# Sources
SRCS    = $(wildcard $(SRC_PATH)/*.cpp)
SRCS_FL = $(wildcard $(SRC_PATH)/fl/*.cpp)

# Windows resource
WRES = res/resource.rc

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)
OBJS_FL = $(SRCS_FL:$(SRC_PATH)/fl/%.cpp=$(TARGET_OBJ)/fl/%.o)
WROBJ = $(TARGET_OBJ)/resource.o

.PHONY: prepare clean

all: prepare clean continue packaging

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)
	@mkdir -p $(TARGET_OBJ)/fl

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o
	@rm -rf $(TARGET_OBJ)/fl/*.o

packaging:
	@cp -rf LICENSE ${TARGET_DIR}
	@cp -rf readme.md ${TARGET_DIR}

$(OBJS_FL): $(SRCS_FL)
	@echo "Building $@ ... "
	@$(GCC) $(CFLAGS) -c $< -o $@

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(WROBJ): $(WRES)
	@echo "Building windows resource ..."
	@$(WRC) -i $(WRES) $(WFLAGS) -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS) $(OBJS_FL) $(WROBJ)
	@echo "Generating $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(TARGET_OBJ)/fl/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
