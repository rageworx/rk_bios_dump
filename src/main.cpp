#include <windows.h>
#include <tchar.h>

#include <io.h>

#include <FL/fl_draw.h>
#include <FL/Fl.H>
#if (FL_ABI_VERSION < 10304)
    #error "This program requires minimal FLTK 1.3.4"
#endif
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Group.H>
#include "Fl_DropFileGroup.H"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "resource.h"
#include "winbiosinfo.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

static Fl_Double_Window*    window = NULL;
static Fl_Box*              boxState = NULL;
static Fl_Button*           btnCheck = NULL;
static Fl_Text_Display*     logDisplay = NULL;
static Fl_Text_Buffer*      logBuffer = NULL;
static Fl_DropFileGroup*    grpDivSec1 = NULL;
static Fl_Group*            grpDivSec2 = NULL;
static bool                 isRunning = false;
static HANDLE               hThreadMain = INVALID_HANDLE_VALUE;

static string               refstrBINfilename;

const char* convLoc = NULL;
const char* convLng = NULL;

////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI threadMain( LPVOID p );

////////////////////////////////////////////////////////////////////////////////

void addLog( const char* t )
{
    int prevl = logDisplay->count_lines(0, logDisplay->buffer()->length(), 1);

    Fl::lock();
    logBuffer->append( t );

    logDisplay->insert_position( logDisplay->buffer()->length() );

    bool redrawneed = false;
    int nextl = logDisplay->count_lines(0, logDisplay->buffer()->length(), 1);

    if ( prevl != nextl )
    {
        redrawneed = true;
        logDisplay->scroll( logDisplay->count_lines(0, logDisplay->buffer()->length(), 1), 0 );
    }

    logDisplay->redraw();
    Fl::unlock();

    if ( redrawneed == true )
    {
        window->redraw();

        Fl::lock();
        Fl::flush();
        Fl::unlock();
    }
}

void startProcHosts()
{
    if (  isRunning == true )
    {
        addLog( "Error: Already process in running !\n" );
        return;
    }

    isRunning = true;

    addLog( "\n" );
    addLog( "=====================================\n" );
    addLog( "\n" );

    hThreadMain = CreateThread( NULL,
                                0,
                                threadMain,
                                NULL,
                                0,
                                NULL );

    if ( hThreadMain == INVALID_HANDLE_VALUE )
    {
        addLog( "Error: Failed to create process!\n" );
        isRunning = false;
    }
}

void stopProcHosts()
{
    if ( isRunning == true )
    {
        if ( hThreadMain != INVALID_HANDLE_VALUE )
        {
            addLog( "-> Terminating process ... " );

            TerminateThread( hThreadMain, 0 );
            WaitForSingleObject( hThreadMain, INFINITE );
            CloseHandle( hThreadMain );

            addLog( "Done.\n" );
        }

        isRunning = false;
    }
}

void fl_w_cb( Fl_Widget* w, void* p )
{
    if ( w == window )
    {
        if ( isRunning == true )
        {
            int reti =  MessageBox( fl_xid_( window ),
                                    TEXT("Now processing dump, Do you really want to quit?"),
                                    TEXT("WARNING !"),
                                    MB_ICONWARNING | MB_YESNO );
            if ( reti == IDYES )
            {
                stopProcHosts();
                window->hide();
            }
        }
        else
        {
            window->hide();
        }
    }
    else
    if ( w == btnCheck )
    {
        grpDivSec1->deactivate();
        btnCheck->deactivate();
        startProcHosts();
    }
    else
    if ( w == grpDivSec1 )
    {
        if ( ( isRunning == false ) || ( hThreadMain == INVALID_HANDLE_VALUE ) )
        {
            refstrBINfilename = grpDivSec1->filepath();

            if ( refstrBINfilename.size() > 0 )
            {
                startProcHosts();
            }
        }
        else
        {
            MessageBox( fl_xid_( window ),
                        TEXT("Process already in running !"),
                        TEXT("Notice"),
                        MB_ICONINFORMATION );
        }
    }
}

void applyThemeColor( Fl_Widget* w )
{
    if ( w != NULL )
    {
        w->box( FL_FLAT_BOX );
        w->color( 0x66666600 );
        w->labelcolor( 0xEEEEEE00 );
        w->labelfont( FL_FREE_FONT );
        w->labelsize( 11 );
        w->clear_visible_focus();
    }
}

void createSubComponents()
{
    grpDivSec1 = new Fl_DropFileGroup( 0, 0, 500, 40 );
    if ( grpDivSec1 != NULL )
    {
        grpDivSec1->callback( fl_w_cb, NULL );
        grpDivSec1->color( 0x88666600 );
        grpDivSec1->labelcolor( FL_WHITE );
        grpDivSec1->labelsize( 12 );
        grpDivSec1->begin();
    }

    btnCheck = new Fl_Button( 10, 10, 300, 20, "Start BIOS dump" );
    if ( btnCheck != NULL )
    {
        applyThemeColor( btnCheck );
        btnCheck->box( FL_THIN_UP_BOX );
        btnCheck->callback( fl_w_cb, NULL );
    }

    if ( grpDivSec1 != NULL )
    {
        grpDivSec1->end();
    }

    grpDivSec2 = new Fl_Group( 0, 40, 500, 450 );
    if ( grpDivSec2 != NULL )
    {
        grpDivSec2->begin();
    }

    logDisplay = new Fl_Text_Display( 10, 40, 480, 450 );
    if ( logDisplay != NULL )
    {
        logBuffer = new Fl_Text_Buffer();
        if ( logBuffer != NULL )
        {
            logDisplay->box( FL_THIN_DOWN_BOX );
            logDisplay->color( FL_BLACK );
            logDisplay->textfont( FL_COURIER );
            logDisplay->textsize( 8 );
            logDisplay->buffer( logBuffer );
            logDisplay->scrollbar_width(9);
            logDisplay->scrollbar_box( FL_THIN_UP_BOX );
            logDisplay->scrollbar_color( 0x66666600 );
            logDisplay->textcolor( FL_GREEN );
            logDisplay->textsize(11);
            logDisplay->wrap_mode( Fl_Text_Display::WRAP_AT_BOUNDS, 2 );
        }
    }

    if ( grpDivSec2 != NULL )
    {
        grpDivSec2->end();
    }
}

bool createWindowComponents()
{
    int versionsi[] = {APP_VERSION};

    if ( window == NULL )
    {
        char wintitlestr[256] = {0};

        sprintf( wintitlestr,
                 "Rageworx's BIOS dumper, version %d.%d.%d.%d",
                 versionsi[0],
                 versionsi[1],
                 versionsi[2],
                 versionsi[3] );

        window = new Fl_Double_Window( 500, 500, wintitlestr );
        if ( window != NULL )
        {
            applyThemeColor( window );
            window->begin();
            createSubComponents();

            if ( grpDivSec2 != NULL )
            {
                window->resizable( grpDivSec2 );
                window->size_range( window->w(),
                                    window->h(),
                                    window->w(),
                                    0,
                                    0,
                                    0,
                                    0 );
            }

            window->end();
            window->icon((char*)LoadIcon(fl_display, MAKEINTRESOURCE( IDC_ICON_A ) ) );
            window->callback( fl_w_cb, NULL );
            window->show();

            return true;
        }
    }

    return false;
}

void destroyAllComponents()
{
    // Nothing to do, automatically unrefered.
}

void gethexstrs( const unsigned char* refs, int sz, char* out )
{
    if ( ( refs == NULL ) || ( sz <= 0 ) )
        return;

    for( int cnt=0; cnt<sz; cnt++ )
    {
        char tmpstr[10] = {0};

        if ( ( refs[cnt] >= 0x20 ) && ( refs[cnt] <= 0x7F ) )
        {
            out[cnt] = refs[cnt];
        }
        else
        {
            out[cnt] = '.';
        }
    }
}

void printinhex( const unsigned char* ref, int refsz )
{
    const unsigned prtwidth = 15;
    char tmpstr[64] = {0};
    char prtstr[128] = {0};
    int  rcnt = 0;
    char humancode[32] = {0};

    for( int cnt=0; cnt<refsz; cnt++ )
    {
        if ( cnt+1 < refsz )
        {
            snprintf( tmpstr, 256,"%02X ", ref[cnt] );
            strcat( prtstr, tmpstr );

            rcnt++;

            if ( rcnt > prtwidth )
            {
                memset( humancode, 0, 32 );
                gethexstrs( &ref[cnt-prtwidth], prtwidth, humancode );
                snprintf( tmpstr, 256,"%s\n", humancode );
                rcnt = 0;

                strcat( prtstr, tmpstr );
                addLog( prtstr );

                memset( prtstr, 0, 128 );
            }
        }
    }

    if ( rcnt > 0 )
    {
        for( int cnt=0; cnt<(17 - rcnt); cnt++)
        {
            strcat( prtstr, "   " );
        }

        memset( humancode, 0, 32 );
        gethexstrs( &ref[ refsz - rcnt - 2 ], rcnt, humancode );

        strcat( prtstr, humancode );
        strcat( prtstr, "\n");

        addLog( prtstr );
    }
}

bool PrintBIOSInfo( Win32BIOSinfomation::BIOSInfo* pBIOS )
{
    if ( pBIOS == NULL )
        return false;

    char tmpstr[256] = {0};

    addLog( "\n" );
    addLog( "== BIOS information section ==\n" );
    addLog( "\n" );

	snprintf( tmpstr, 256,"BIOS Starting Segment: 0x%X\n", pBIOS->StartSegment );
	addLog( tmpstr );

	if ( pBIOS->VendorString != NULL )
    {
        snprintf( tmpstr, 256,"Vendor: %s\n", pBIOS->VendorString );
        addLog( tmpstr );
    }

    if ( pBIOS->VersionString != NULL )
    {
        snprintf( tmpstr, 256,"Version: %s\n",  pBIOS->VersionString );
        addLog( tmpstr );
    }

    if ( pBIOS->ReleaseString != NULL )
    {
        snprintf( tmpstr, 256,"Release Date: %s\n", pBIOS->ReleaseString );
        addLog( tmpstr );
    }

	snprintf( tmpstr, 256,"Image Size: %dK\n", (pBIOS->ROMsize + 1) * 64);addLog( tmpstr );

	if ( pBIOS->Header.Length > 0x14) /// maybe BIOS spec 2.4 later.
	{
		snprintf( tmpstr, 256,"System BIOS version: %d.%d\n", pBIOS->ReleaseMajor, pBIOS->ReleaseMinor);
		addLog( tmpstr );
		snprintf( tmpstr, 256,"EC Firmware version: %d.%d\n", pBIOS->EFMajor, pBIOS->EFMinor);
		addLog( tmpstr );
	}

	addLog( "\n" );
	return true;
}

void PrintSysInfo( Win32BIOSinfomation::SystemInfo* pSystem )
{
    if ( pSystem == NULL )
        return;

    char tmpstr[256] = {0};

    addLog( "\n" );
    addLog( "== System information section ==\n" );
    addLog( "\n" );

	snprintf( tmpstr, 256,"Manufacturer: %s\n", pSystem->ManufacturerString );addLog( tmpstr );
	snprintf( tmpstr, 256,"Product Name: %s\n", pSystem->ProductNameString );addLog( tmpstr );
	snprintf( tmpstr, 256,"Version: %s\n", pSystem->VersionString );addLog( tmpstr );
	snprintf( tmpstr, 256,"Serial Number: %s\n", pSystem->SerialNumberString );addLog( tmpstr );

	if ( pSystem->Header.Length > 0x08 ) /// maybe 2.1 later
	{
        sprintf( tmpstr,
                 "UUID: %02X%02X%02X%02X-"
                       "%02X%02X-"
                       "%02X%02X-"
                       "%02X%02X-"
                       "%02X%02X%02X%02X%02X%02X\n",
                 (uchar)pSystem->UUID[0],
                 (uchar)pSystem->UUID[1],
                 (uchar)pSystem->UUID[2],
                 (uchar)pSystem->UUID[3],
                 (uchar)pSystem->UUID[4],
                 (uchar)pSystem->UUID[5],
                 (uchar)pSystem->UUID[6],
                 (uchar)pSystem->UUID[7],
                 (uchar)pSystem->UUID[8],
                 (uchar)pSystem->UUID[9],
                 (uchar)pSystem->UUID[10],
                 (uchar)pSystem->UUID[11],
                 (uchar)pSystem->UUID[12],
                 (uchar)pSystem->UUID[13],
                 (uchar)pSystem->UUID[14],
                 (uchar)pSystem->UUID[15] );
        addLog( tmpstr );
	}

	if ( pSystem->Header.Length > 0x19 ) /// spec 2.4 later
	{
		snprintf( tmpstr, 256,"SKU Number: %s\n", pSystem->SKUNumberString );addLog( tmpstr );
		snprintf( tmpstr, 256,"Family: %s\n", pSystem->FamilyString );addLog( tmpstr );
	}
}

void PrintBoardInfo( Win32BIOSinfomation::BoardInfo* pBoard )
{
    if ( pBoard == NULL )
        return;

    char tmpstr[256] = {0};

    addLog( "\n" );
    addLog( "== Board information section ==\n" );
    addLog( "\n" );

	snprintf( tmpstr, 256,"Manufacturer: %s\n", pBoard->ManufacturerString );addLog( tmpstr );
	snprintf( tmpstr, 256,"Product Name: %s\n", pBoard->ProductNameString );addLog( tmpstr );
	snprintf( tmpstr, 256,"Version: %s\n", pBoard->VersionString );addLog( tmpstr );
	snprintf( tmpstr, 256,"SKU : %s\n", pBoard->SKUNumberString );addLog( tmpstr );
}

void PrintProcessorInfo( Win32BIOSinfomation::ProcessorInfo* processor )
{
    if ( processor == NULL )
        return;

    char tmpstr[256] = {0};

    addLog( "\n" );
    addLog( "== Processor information section ==\n" );
    addLog( "\n" );

	snprintf( tmpstr, 256,"Type: %u, Faimily: %u(%u), ID : %02X%02X%02X%02X%02X%02X%02X%02X\n",
              processor->ProcessorType,
              processor->ProcessorFamily,
              processor->ProcessorFamily2,
              processor->ProcessorID[0],
              processor->ProcessorID[1],
              processor->ProcessorID[2],
              processor->ProcessorID[3],
              processor->ProcessorID[4],
              processor->ProcessorID[5],
              processor->ProcessorID[6],
              processor->ProcessorID[7] );
	addLog( tmpstr );

    snprintf( tmpstr, 256, "Socket: %s\n", processor->SocketDesignationString );
    addLog( tmpstr );
    snprintf( tmpstr, 256, "Manufacturer: %s\n", processor->ProcessorManufacturerString );
    addLog( tmpstr );
    snprintf( tmpstr, 256, "Version: %s\n", processor->ProcessorVersionString );
    addLog( tmpstr );
    snprintf( tmpstr, 256, "Serial Number: %s\n", processor->SerialNumberString );
    addLog( tmpstr );
    snprintf( tmpstr, 256, "Part Number: %s\n", processor->PartNumberString );
    addLog( tmpstr );

}

void PrintPMAInfo( Win32BIOSinfomation::PhysMemArrayInfo* pma )
{
    if ( pma == NULL )
        return;

    char tmpstr[256] = {0};

    addLog( "\n" );
    addLog( "== Physical Memory Array Information  ==\n" );
    addLog( "\n" );

    QWORD memsz = pma->ExtendedMaxCapa + pma->MaxCapa;

    snprintf( tmpstr, 256, "Memory size: %u MiB x %u\n",
              memsz / 1024,
              pma->NumMemDevs );
    addLog( tmpstr );
}

DWORD WINAPI threadMain( LPVOID p )
{
    logBuffer->remove( 0, logBuffer->length() );

    char  libwin32ver[32] = {0};
    char* thisPCname = getenv( "COMPUTERNAME" );

    Win32BIOSinfomation biosinfo;
    biosinfo.getVersion( libwin32ver );

    addLog( "Profile name : " );
    addLog( thisPCname );
    addLog( "\n" );

    addLog( "Raph.Kay CMOS/EFI/BIOS RSMB dump & parse engine, WIN32\n" );
    addLog( "(C)2016, rageworx@gmail.com\n" );
    addLog( "\n" );
    addLog( "libwin32bios version : " );
    addLog( libwin32ver );
    addLog( "\n" );
    addLog( "\n" );


    if ( refstrBINfilename.size() > 0 )
    {
        addLog( "BIN file load : " );
        addLog( refstrBINfilename.c_str() );
        addLog( "\n\n" );

        if ( biosinfo.loadfrombin( refstrBINfilename.c_str() ) == false )
        {
            MessageBox( fl_xid_( window ),
                        TEXT("Could not load BIOS information from file !"),
                        TEXT("Error."),
                        MB_ICONERROR );

            isRunning = false;
            grpDivSec1->activate();
            hThreadMain = INVALID_HANDLE_VALUE;
            return 0;
        }
    }
    else
    {
        biosinfo.loadfromRSMB();
    }

    if ( biosinfo.RSMBsize() > 0 )
    {
        Win32BIOSinfomation::RawSMBIOSData* SMB = biosinfo.SMBIOSdata();

        if ( SMB != NULL )
        {
            addLog( "== BIOS (RSMB) dumped ==\n" );
            addLog( "\n" );
            printinhex( SMB->SMBIOSTableData, SMB->Length );
            addLog( "\n" );

            char tmpstr[256] = {0};

            addLog( "\n" );
            addLog( "== BIOS parsed information ==\n" );
            addLog( "\n" );
            snprintf( tmpstr, 256," Calling method = 0x%02X\n", SMB->Used20CallingMethod );
            addLog( tmpstr );
            snprintf( tmpstr, 256," Version        = %d.%d\n", SMB->MajorVersion, SMB->MinorVersion );
            addLog( tmpstr );
            snprintf( tmpstr, 256," DMI revision   = %d\n", SMB->DMIRevision );
            addLog( tmpstr );
            snprintf( tmpstr, 256," data size      = %d bytes\n", SMB->Length );
            addLog( tmpstr );
            addLog( "\n" );

            PrintBIOSInfo( biosinfo.BIOSinfo() );
            PrintSysInfo( biosinfo.SystemInformation() );
            PrintBoardInfo( biosinfo.BoardInformation() );
            PrintProcessorInfo( biosinfo.ProcessorInformation() );
            PrintPMAInfo( biosinfo.PhysicalMemoryArrayInfo() );
        }
        else
        {
            addLog( "Failure: RSMB. SMBIOS information may not read.\n" );
        }
    }
    else
    {
        addLog( "Failure: WIN32 API, GetSystemFirmwareTable() may failure.\n" );
    }

    Sleep( 1000 );

    if ( refstrBINfilename.size()  == 0 )
    {
        addLog( "-> Writing current information ... \n");

        char tmpfname[256] = {0};
        sprintf( tmpfname, "%s_bios_dump.txt", thisPCname );

        logBuffer->savefile( tmpfname );

        sprintf( tmpfname, "%s_bios_dump.bin", thisPCname );

        biosinfo.dumptofile( tmpfname );

    }

    addLog( "\n" );
    addLog( "-> All process done.\n" );

    if ( refstrBINfilename.size() > 0 )
    {
        refstrBINfilename.clear();
    }

    isRunning = false;
    grpDivSec1->activate();
    hThreadMain = INVALID_HANDLE_VALUE;
    return 0;
}

void procAutoLocale()
{
    LANGID currentUIL = GetSystemDefaultLangID();

    switch( currentUIL & 0xFF )
    {
        case LANG_KOREAN:
            convLoc = "Korean_Korea.949";
            convLng = "Malgun Gothic";
            break;

        case LANG_JAPANESE:
            convLoc = "Japanese_Japan.932";
            convLng = "Meiryo";
            break;

        case LANG_CHINESE:
            convLoc = "Chinese_China.936";
            convLng = "Microsoft YaHei";
            break;

        case LANG_CHINESE_TRADITIONAL:
            convLoc = "Chinese (Traditional)_Taiwan.950";
            convLng = "Microsoft JhengHei";
            break;

        default:
            convLoc = "C";
            convLng = "Tahoma";
            break;
    }

    setlocale( LC_ALL, convLoc );
}

int main (int argc, char ** argv)
{
    int reti = 0;

    procAutoLocale();

    Fl::scheme( "flat" );

    Fl::set_font( FL_FREE_FONT, convLoc );
    Fl_Double_Window::default_xclass( "DRTECHBIOSDMP" );
    Fl::lock();

    if ( createWindowComponents() == true )
    {
        addLog( "RAGEWORX WINDOWS 32/64 BIOS DUMP Open Source Example.\n" );
        addLog( "(C)Copyright 2016, 2017 Raphael Kim (rageworx@gmail.com)\n" );
        addLog( "\n" );
        addLog( "\n" );

        reti = Fl::run();

        destroyAllComponents();
    }

    return reti;
}
