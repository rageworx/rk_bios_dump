#include "winbiosinfo.h"

#include <io.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>

// -----------------------------------------------------------------------------

// "RSMB"
#define DEF_SMBIOS_RSMB             0x52534D42

#define MACRO_DEALLOCSTR( _x_ )     if ( _x_ != NULL ) delete[] _x_; _x_ = NULL

#ifndef _MAX
    #define _MAX(a,b)    (((a)>(b))?(a):(b))
#endif

// -----------------------------------------------------------------------------

Win32BIOSinfomation::Win32BIOSinfomation()
 : rsmbBufferSz ( 0 ),
   SMB( NULL ),
   pbuff ( NULL ),
   cacheCount( 0 ),
   portConCount( 0 ),
   sysSlotCount( 0 ),
   biosinforeadn( false ),
   current_result( RESULT_OK )
{
    clearbuffers();
}

Win32BIOSinfomation::~Win32BIOSinfomation()
{
    deallocbuffers();
}

void Win32BIOSinfomation::clearbuffers()
{
    memset( rsmbBuffer, 0, RSMB_BUFFER_SIZE );

    biosinforeadn = false;
    memset( &biosInfo, 0, sizeof( BIOSInfo ) );
    memset( &systemInfo, 0, sizeof( SystemInfo ) );
    memset( &boardInfo, 0, sizeof( BoardInfo ) );
    memset( &enclosureInfo, 0, sizeof( EnclosureInfo ) );
    memset( &processorInfo, 0, sizeof( ProcessorInfo ) );
    memset( &memCtrlrInfo, 0, sizeof( MemCtrlrInfo ) );
    memset( &memModuleInfo, 0, sizeof( MemModuleInfo ) );
    memset( &portConInfo, 0, sizeof( PortConInfo ) );
    memset( &sysSlotsInfo, 0, sizeof( SysSlotsInfo ) );
    memset( &sysResetInfo, 0, sizeof( SysResetInfo ) );
    memset( &physMemArrayInfo, 0, sizeof( PhysMemArrayInfo ) );

    cacheCount = 0;
    for( int cnt=0; cnt<MAX_CACHE_COUNT; cnt++ )
    {
        memset( &cacheInfo[cnt], 0, sizeof( CacheInfo ) );
    }

    portConCount = 0;
    for( int cnt=0; cnt<MAX_PORT_COUNT; cnt++ )
    {
        memset( &portConInfo[cnt], 0, sizeof( PortConInfo ) );
    }

    sysSlotCount = 0;
    for( int cnt=0; cnt<MAX_DEVICESLOT_COUNT; cnt++ )
    {
        memset( &sysSlotsInfo[cnt], 0, sizeof( SysSlotsInfo ) );
    }

    memDeviceCount = 0;
    for( int cnt=0; cnt<MAX_MEMORY_COUNT; cnt++ )
    {
        memset( &memDeviceInfo[cnt], 0, sizeof( MemoryDeviceInfo ) );
    }
}

void Win32BIOSinfomation::deallocbuffers()
{
    // biosInfo
    MACRO_DEALLOCSTR( biosInfo.VendorString );
    MACRO_DEALLOCSTR( biosInfo.VersionString );
    MACRO_DEALLOCSTR( biosInfo.ReleaseString );

    // systemInfo
    MACRO_DEALLOCSTR( systemInfo.ManufacturerString );
    MACRO_DEALLOCSTR( systemInfo.ProductNameString );
    MACRO_DEALLOCSTR( systemInfo.VersionString );
    MACRO_DEALLOCSTR( systemInfo.SerialNumberString );
    MACRO_DEALLOCSTR( systemInfo.SKUNumberString );
    MACRO_DEALLOCSTR( systemInfo.FamilyString );

    // BoardInfo
    MACRO_DEALLOCSTR( boardInfo.ManufacturerString );
    MACRO_DEALLOCSTR( boardInfo.ProductNameString );
    MACRO_DEALLOCSTR( boardInfo.VersionString );
    MACRO_DEALLOCSTR( boardInfo.SKUNumberString );
    MACRO_DEALLOCSTR( boardInfo.AssetTagString );
    MACRO_DEALLOCSTR( boardInfo.LICString );

    // EnclosureInfo
    MACRO_DEALLOCSTR( enclosureInfo.ManufacturerString );
    MACRO_DEALLOCSTR( enclosureInfo.VersionString );
    MACRO_DEALLOCSTR( enclosureInfo.SerialNumberString );
    MACRO_DEALLOCSTR( enclosureInfo.AssetTagString );
    MACRO_DEALLOCSTR( enclosureInfo.SKUString );

    // ProcessorInfo
    MACRO_DEALLOCSTR( processorInfo.SocketDesignationString );
    MACRO_DEALLOCSTR( processorInfo.ProcessorManufacturerString );
    MACRO_DEALLOCSTR( processorInfo.ProcessorVersionString );
    MACRO_DEALLOCSTR( processorInfo.SerialNumberString );
    MACRO_DEALLOCSTR( processorInfo.AssetTagString );
    MACRO_DEALLOCSTR( processorInfo.PartNumberString );
    MACRO_DEALLOCSTR( processorInfo.UnknownString );

    // MemModuleInfo
    MACRO_DEALLOCSTR( memModuleInfo.SocketDesignString );

    // CacheInfo
    for ( unsigned cnt=0; cnt<cacheCount; cnt++ )
    {
        MACRO_DEALLOCSTR( cacheInfo[cnt].SocketDesignString );
    }

    // PortConInfo
    for ( unsigned cnt=0; cnt<portConCount; cnt++ )
    {
        MACRO_DEALLOCSTR( portConInfo[cnt].InternalRefDesignString );
        MACRO_DEALLOCSTR( portConInfo[cnt].ExternalRefDesignString );
    }

    // SysSlotsInfo
    for ( unsigned cnt=0; cnt<sysSlotCount; cnt++ )
    {
        MACRO_DEALLOCSTR( sysSlotsInfo[cnt].SlotDesignString );
    }

    // MemDeviceInfo
    for ( unsigned cnt=0; cnt<memDeviceCount; cnt++ )
    {
        MACRO_DEALLOCSTR( memDeviceInfo[cnt].DeviceLocator );
        MACRO_DEALLOCSTR( memDeviceInfo[cnt].BankLocator );
        MACRO_DEALLOCSTR( memDeviceInfo[cnt].Manufacturer );
        MACRO_DEALLOCSTR( memDeviceInfo[cnt].SerialNumber );
        MACRO_DEALLOCSTR( memDeviceInfo[cnt].AssetTag );
        MACRO_DEALLOCSTR( memDeviceInfo[cnt].PartNumber );
    }

    // clear lefts.
    clearbuffers();
}

void Win32BIOSinfomation::getVersion( char* retStr )
{
    if ( retStr != NULL )
    {
        strcpy( retStr, "0.3b" );
    }
}

bool Win32BIOSinfomation::loadfromRSMB()
{
    SMB = NULL;

    clearbuffers();

    // Get real requirement size.
    UINT retI = GetSystemFirmwareTable( DEF_SMBIOS_RSMB, 0, 0, 0 );

    if ( retI > 0 )
    {
        rsmbBufferSz = retI;

        retI = GetSystemFirmwareTable( DEF_SMBIOS_RSMB,
                                       0,
                                       rsmbBuffer,
                                       rsmbBufferSz );

        if ( retI <= rsmbBufferSz )
        {
            SMB = (RawSMBIOSData*)rsmbBuffer;

            return parse();
        }
        else
        {
            rsmbBufferSz = 0;
        }
    }

    return false;
}

bool Win32BIOSinfomation::loadfrombin( const char* fn )
{
    if ( fn != NULL )
    {
        size_t   wconvl = strlen( fn ) + 1;
        wchar_t* wconvs = new wchar_t[ wconvl ];

        if ( wconvs != NULL )
        {
            mbstowcs( wconvs, fn, wconvl );

            bool retb = loadfrombin( wconvs );

            delete[] wconvs;

            return retb;
        }

    }
    return false;
}

bool Win32BIOSinfomation::dumptofile( const char* fn )
{
    if ( fn != NULL )
    {
        size_t   wconvl = strlen( fn ) + 1;
        wchar_t* wconvs = new wchar_t[ wconvl ];

        if ( wconvs != NULL )
        {
            mbstowcs( wconvs, fn, wconvl );

            bool retb = dumptofile( wconvs );

            delete[] wconvs;

            return retb;
        }

    }
    return false;
}

bool Win32BIOSinfomation::loadfrombin( const wchar_t* fn )
{
    if ( _waccess( fn, F_OK ) == 0 )
    {
        FILE* fp = _wfopen( fn, L"rb" );

        if ( fp == NULL )
            return false;

        fseek( fp, 0, SEEK_END );
        int flen = ftell( fp );
        fseek( fp, 0, SEEK_SET );

        if ( flen < RSMB_BUFFER_SIZE )
        {
            SMB = NULL;

            clearbuffers();

            flen = RSMB_BUFFER_SIZE;

            fread( rsmbBuffer, 1, flen, fp );
            fclose( fp );

            rsmbBufferSz = flen;

            SMB = (RawSMBIOSData*)rsmbBuffer;

            return parse();
        }
    }

    return false;
}

bool Win32BIOSinfomation::dumptofile( const wchar_t* fn )
{
    if ( _waccess( fn, F_OK ) == 0 )
    {
        if ( _wunlink( fn ) != 0 )
            return false;
    }

    if ( SMB == NULL )
        return false;

    FILE* fp = _wfopen( fn, L"wb" );
    if ( fp != NULL )
    {
        fwrite( rsmbBuffer, 1, rsmbBufferSz , fp );
        fclose( fp );
        return true;
    }

    return false;
}

bool Win32BIOSinfomation::parse()
{
    if ( SMB == NULL )
        return false;

    PBYTE pbuffstart = SMB->SMBIOSTableData;

    pbuff = SMB->SMBIOSTableData;
    pbuff_end = pbuff + SMB->Length;

    int prev_type = -1;

    while( true )
    {
        SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;
#ifdef DEBUG
        printf( "mem.queue = 0x%04X / 0x%04X | ",
                pbuff - pbuffstart, pbuff_end - pbuffstart );
        printf("current BIOS header : 0x%02X(%d)\n",
                refheader->Type, refheader->Type );
        printf("%08X : ", ( pbuff - pbuffstart ) + ( pbuffstart - rsmbBuffer) );
        printinhex( pbuff, 16 );
        printf( "\n" );
#endif // DEBUG

        if ( prev_type != 7 )
            if ( prev_type == refheader->Type )
                break;

        prev_type = refheader->Type;

        switch ( refheader->Type )
        {
            case 0:
                remapBIOSInfo();
                break;

            case 1:
                remapSystemInfo();
                break;

            case 2:
                remapBoardInfo();
                break;

            case 3:
                remapEnclosureInfo();
                break;

            case 4:
                remapProcessorInfo();
                break;

            case 5:
                remapMemCtrlrInfo();
                break;

            case 6:
                remapMemModuleInfo();
                break;

            case 7:
                remapCacheInfo();
                break;

            case 8:
                remapPortConnectorInfo();
                break;

            case 9:
                remapSysSlotsInfo();
                break;

            case 10:
                skipUnsupportedInfo();
                break;

            case 11:
                skipUnsupportedInfo();
                break;

            case 16:
                remapPhysMemArrayInfo();
                break;

            case 17:
                remapMemoryDeviceInfo();
                break;

            case 23:
                remapSystemReset();
                break;

            default:
                skipUnsupportedInfo();
                break;

        }

        if ( pbuff >= pbuff_end )
            break;

    }

    return true;
}

Win32BIOSinfomation::PortConInfo* Win32BIOSinfomation::PortConnectorInformation( unsigned index )
{
    if ( index < portConCount )
    {
        return &portConInfo[ index ];
    }

    return NULL;
}

Win32BIOSinfomation::CacheInfo* Win32BIOSinfomation::CacheInformation( unsigned index )
{
    if ( index < cacheCount )
    {
        return &cacheInfo[ index ];
    }

    return NULL;
}

Win32BIOSinfomation::SysSlotsInfo* Win32BIOSinfomation::SystemSlotsInformation( unsigned index )
{
    if ( index < sysSlotCount )
    {
        return &sysSlotsInfo[ index ];
    }

    return NULL;
}

Win32BIOSinfomation::MemoryDeviceInfo* Win32BIOSinfomation::MemoryDeviceInformation( unsigned index )
{
    if ( index < memDeviceCount )
    {
        return &memDeviceInfo[ index ];
    }

    return NULL;
}

PCHAR Win32BIOSinfomation::getPString( PBYTE pbData )
{
	PCHAR pStr = NULL;

	BYTE cnt = 0;

    pStr    = (PCHAR)pbData;
    // Overflow check.
    if ( pStr != NULL )
    {
        PBYTE pbCmpChk = pbData;
        pbCmpChk += ( strlen( pStr ) + 1 );
        if ( ( pbCmpChk - pbData ) < 255 )
        {
            pbData += ( strlen( pStr ) + 1 );
        }
        else
        {
            pStr = NULL;
        }
    }
    else
    {
        pStr = NULL;
    }

	return pStr;
}

void Win32BIOSinfomation::getPStringPtrs( UINT sz, remapstringptrs &rets )
{
    static char nodatastring[] = "(none)";

    if ( ( sz >  0 ) && ( sz < MAX_RETURN_STRING_POINTERS - 1 ) )
    {
        int cnt = 0;

        for ( cnt=0; cnt<sz; cnt++ )
        {
            rets.strptr[ cnt ] = NULL;
        }

        rets.strptr[ 0 ] = nodatastring;

        rets.sizes = 0;

        for ( cnt=0; cnt<sz; cnt++ )
        {
            if ( *pbuff == 0x00 )
                break;

            PCHAR retptr = getPString( pbuff );
            if ( retptr != NULL )
            {
                int rstrsz = strlen ( retptr );

                rets.strptr[ cnt + 1 ] = retptr;

                pbuff += rstrsz + 1;
            }
        }

        rets.sizes = sz;
    }
}

void Win32BIOSinfomation::remapBIOSInfo( )
{
    if ( pbuff == NULL )
        return;

    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        unsigned fixedlen = refheader->Length;
        unsigned maxsidx  = 3;

        if ( biosinforeadn == true )
        {
            pbuff += fixedlen;
            skipStringInfo();
            pbuff++;

            return;
        }

        memcpy( &biosInfo, refheader, fixedlen );

        pbuff += fixedlen;

        remapstringptrs retstrs;

        if ( biosInfo.VendorStrIndex  < 0xFF )
            maxsidx = _MAX( maxsidx, biosInfo.VendorStrIndex );
        if( biosInfo.VersionStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, biosInfo.VersionStrIndex );
        if ( biosInfo.ReleaseStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, biosInfo.ReleaseStrIndex );

        getPStringPtrs( maxsidx, retstrs );

        if ( biosInfo.VendorStrIndex  < 0xFF )
            biosInfo.VendorString  = safestrcast( retstrs.strptr[ biosInfo.VendorStrIndex ] );
        if( biosInfo.VersionStrIndex < 0xFF )
            biosInfo.VersionString = safestrcast( retstrs.strptr[ biosInfo.VersionStrIndex ] );
        if ( biosInfo.ReleaseStrIndex < 0xFF )
            biosInfo.ReleaseString = safestrcast( retstrs.strptr[ biosInfo.ReleaseStrIndex ] );

        biosinforeadn = true;

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void  Win32BIOSinfomation::remapSystemInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    PBYTE startptr = pbuff;
    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        unsigned fixedlen  = refheader->Length;
        unsigned readstrsz = 4;
        float    smbver    = SMB->MajorVersion + (float)( SMB->MinorVersion / 10.f );

        if ( smbver > 2.0f )
        {
            int cplen = sizeof( SMBIOSHEADER ) + 4;
            memcpy( &systemInfo, refheader, cplen );

            pbuff += cplen;
        }

        if ( smbver >= 2.1f )
        {
            memcpy( systemInfo.UUID, pbuff, 16 );
            pbuff += 16;

            systemInfo.WakeUpType = *pbuff++;
        }

        if ( smbver >= 2.4f )
        {
            systemInfo.SKUNumberStrIndex = *pbuff++;
            systemInfo.FamilyStrIndex = *pbuff++;

            readstrsz += 2;
        }

        // check max indexes.
        if ( systemInfo.ManufacturerStrIndex < 0xFF )
            readstrsz = _MAX( readstrsz, systemInfo.ManufacturerStrIndex );
        if ( systemInfo.ProductNameStrIndex < 0xFF )
            readstrsz = _MAX( readstrsz, systemInfo.ProductNameStrIndex );
        if ( systemInfo.VersionStrIndex < 0xFF )
            readstrsz = _MAX( readstrsz, systemInfo.VersionStrIndex );
        if ( systemInfo.SerialNumberStrIndex < 0xFF )
            readstrsz = _MAX( readstrsz, systemInfo.SerialNumberStrIndex );
        if ( systemInfo.SKUNumberStrIndex < 0xFF )
            readstrsz = _MAX( readstrsz, systemInfo.SKUNumberStrIndex );
        if ( systemInfo.FamilyStrIndex < 0xFF )
            readstrsz = _MAX( readstrsz, systemInfo.FamilyStrIndex );

        pbuff = startptr;
        pbuff += fixedlen;

        remapstringptrs retstrs;

        getPStringPtrs( readstrsz, retstrs );

        if ( systemInfo.ManufacturerStrIndex < 0xFF )
            systemInfo.ManufacturerString  = safestrcast( retstrs.strptr[ systemInfo.ManufacturerStrIndex ] );
        if ( systemInfo.ProductNameStrIndex < 0xFF )
            systemInfo.ProductNameString   = safestrcast( retstrs.strptr[ systemInfo.ProductNameStrIndex ] );
        if ( systemInfo.VersionStrIndex < 0xFF )
            systemInfo.VersionString       = safestrcast( retstrs.strptr[ systemInfo.VersionStrIndex ] );
        if ( systemInfo.SerialNumberStrIndex < 0xFF )
            systemInfo.SerialNumberString  = safestrcast( retstrs.strptr[ systemInfo.SerialNumberStrIndex ] );
        if ( systemInfo.SKUNumberStrIndex < 0xFF )
            systemInfo.SKUNumberString     = safestrcast( retstrs.strptr[ systemInfo.SKUNumberStrIndex ] );
        if ( systemInfo.FamilyStrIndex < 0xFF )
            systemInfo.FamilyString        = safestrcast( retstrs.strptr[ systemInfo.FamilyStrIndex ] );

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void Win32BIOSinfomation::remapBoardInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        int fixedlen = refheader->Length;
        unsigned maxsidx  = 6;

        memcpy( &boardInfo, pbuff, fixedlen );

        pbuff += fixedlen;

        remapstringptrs retstrs;

        if ( boardInfo.ManufacturerStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, boardInfo.ManufacturerStrIndex );
        if ( boardInfo.ProductNameStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, boardInfo.ProductNameStrIndex );
        if ( boardInfo.VersionStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, boardInfo.VersionStrIndex );
        if ( boardInfo.SKUNumberStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, boardInfo.SKUNumberStrIndex );
        if ( boardInfo.AssetTagStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, boardInfo.AssetTagStrIndex );
        if ( boardInfo.LocationInChassisStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, boardInfo.LocationInChassisStrIndex );

        getPStringPtrs( maxsidx, retstrs );

        if ( boardInfo.ManufacturerStrIndex < 0xFF )
            boardInfo.ManufacturerString = safestrcast( retstrs.strptr[ boardInfo.ManufacturerStrIndex ] );
        if ( boardInfo.ProductNameStrIndex < 0xFF )
            boardInfo.ProductNameString  = safestrcast( retstrs.strptr[ boardInfo.ProductNameStrIndex ] );
        if ( boardInfo.VersionStrIndex < 0xFF )
            boardInfo.VersionString      = safestrcast( retstrs.strptr[ boardInfo.VersionStrIndex ] );
        if ( boardInfo.SKUNumberStrIndex < 0xFF )
            boardInfo.SKUNumberString    = safestrcast( retstrs.strptr[ boardInfo.SKUNumberStrIndex ] );
        if ( boardInfo.AssetTagStrIndex < 0xFF )
            boardInfo.AssetTagString     = safestrcast( retstrs.strptr[ boardInfo.AssetTagStrIndex ] );
        if ( boardInfo.LocationInChassisStrIndex < 0xFF )
            boardInfo.LICString          = safestrcast( retstrs.strptr[ boardInfo.LocationInChassisStrIndex ] );

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void Win32BIOSinfomation::remapEnclosureInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    PBYTE startptr = pbuff;
    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        unsigned fixedlen = refheader->Length;
        unsigned maxsidx  = 5;
        float    smbver   = SMB->MajorVersion + (float)( SMB->MinorVersion / 10.f );

        if ( smbver >= 2.0f )
        {
            memcpy( &enclosureInfo, pbuff, sizeof( SMBIOSHEADER ) + 5 );
            pbuff += sizeof( SMBIOSHEADER ) + 5;
        }
        else
        {
            pbuff += fixedlen;
        }

        if ( smbver >= 2.1f )
        {
            enclosureInfo.BootupState = *pbuff++;
            enclosureInfo.PowerSupplyState = *pbuff++;
            enclosureInfo.ThermalState = *pbuff++;
            enclosureInfo.SecurityStatus = *pbuff++;
        }

        if ( smbver >= 2.3f )
        {
            memcpy( &enclosureInfo.OEMdefined, pbuff, 4 );
            pbuff += 4;

            enclosureInfo.Height = *pbuff++;
            enclosureInfo.NumberOfPowerCords = *pbuff++;
            enclosureInfo.ContainedElementCount = *pbuff++;
            enclosureInfo.ContainedElementRecordLength = *pbuff++;

            unsigned cpsz = enclosureInfo.ContainedElementCount * enclosureInfo.ContainedElementRecordLength ;

            if ( cpsz > 0 )
            {
                memcpy( enclosureInfo.ContainedElememts, pbuff, cpsz );
                pbuff += cpsz;
            }
        }

        if ( smbver >= 2.7f )
        {
            enclosureInfo.SKUStrIndex = *pbuff++;

            if ( *pbuff == 0x00 )
            {
                memcpy( enclosureInfo.Unknowndatas, pbuff, 2 );
                pbuff += 2;
            }
        }

        remapstringptrs retstrs;

        if ( enclosureInfo.ManufacturerStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, enclosureInfo.ManufacturerStrIndex );
        if ( enclosureInfo.VersionStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, enclosureInfo.VersionStrIndex );
        if ( enclosureInfo.SerialNumberStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, enclosureInfo.SerialNumberStrIndex );
        if ( enclosureInfo.AssetTagStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, enclosureInfo.AssetTagStrIndex );
        if ( enclosureInfo.SKUStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, enclosureInfo.SKUStrIndex );

        getPStringPtrs( maxsidx, retstrs );

        if ( enclosureInfo.ManufacturerStrIndex < 0xFF )
            enclosureInfo.ManufacturerString = safestrcast( retstrs.strptr[ enclosureInfo.ManufacturerStrIndex ] );
        if ( enclosureInfo.VersionStrIndex < 0xFF )
            enclosureInfo.VersionString      = safestrcast( retstrs.strptr[ enclosureInfo.VersionStrIndex ] );
        if ( enclosureInfo.SerialNumberStrIndex < 0xFF )
            enclosureInfo.SerialNumberString = safestrcast( retstrs.strptr[ enclosureInfo.SerialNumberStrIndex ] );
        if ( enclosureInfo.AssetTagStrIndex < 0xFF )
            enclosureInfo.AssetTagString     = safestrcast( retstrs.strptr[ enclosureInfo.AssetTagStrIndex ] );
        if ( enclosureInfo.SKUStrIndex < 0xFF )
            enclosureInfo.SKUString          = safestrcast( retstrs.strptr[ enclosureInfo.SKUStrIndex ] );

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void Win32BIOSinfomation::remapProcessorInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    PBYTE startptr = pbuff;
    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        unsigned fixedlen = refheader->Length;
        unsigned maxsidx = 6;
        float    smbver   = SMB->MajorVersion + (float)( SMB->MinorVersion / 10.f );

        if ( smbver >= 2.0f )
        {
            memcpy( &processorInfo, pbuff, 0x19 );

            pbuff += 0x19;
        }
        else
        {
            pbuff += fixedlen;
        }

        if ( smbver >= 2.1f )
        {
            memcpy( &processorInfo.L1CacheHandle, pbuff, 2 );
            pbuff += 2;

            memcpy( &processorInfo.L2CacheHandle, pbuff, 2 );
            pbuff += 2;

            memcpy( &processorInfo.L3CacheHandle, pbuff, 2 );
            pbuff += 2;
        }

        if ( smbver >= 2.3f )
        {
            processorInfo.SerialNumberStrIndex = *pbuff++;
            processorInfo.AssetTagStrIndex = *pbuff++;
            processorInfo.PartNumberStrIndex = *pbuff++;
        }

        if ( smbver >= 2.5f )
        {
            processorInfo.CoreCount = *pbuff++;
            processorInfo.CoreEnabled = *pbuff++;
            processorInfo.ThreadCount = *pbuff++;

            memcpy( &processorInfo.ProcessorCharacteristics, pbuff, 2 );
            pbuff += 2;
        }

        if ( smbver >= 2.6f )
        {
            memcpy( &processorInfo.ProcessorFamily2, pbuff, 2 );
            pbuff += 2;

        }

        if ( smbver >= 2.7f )
        {
            memcpy( processorInfo.UnknownBytes, pbuff, 7 );
            pbuff += 7;
            maxsidx++;
        }

        remapstringptrs retstrs;

        if ( processorInfo.SocketDesignationStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, processorInfo.SocketDesignationStrIndex );
        if ( processorInfo.ProcessorManufacturerStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, processorInfo.ProcessorManufacturerStrIndex );
        if ( processorInfo.ProcessorVersionStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, processorInfo.ProcessorVersionStrIndex );
        if ( processorInfo.SerialNumberStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, processorInfo.SerialNumberStrIndex );
        if ( processorInfo.AssetTagStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, processorInfo.AssetTagStrIndex );
        if ( processorInfo.PartNumberStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, processorInfo.PartNumberStrIndex );

        getPStringPtrs( maxsidx, retstrs );

        if ( processorInfo.SocketDesignationStrIndex < 0xFF )
            processorInfo.SocketDesignationString      = safestrcast( retstrs.strptr[ processorInfo.SocketDesignationStrIndex ] );
        if ( processorInfo.ProcessorManufacturerStrIndex < 0xFF )
            processorInfo.ProcessorManufacturerString  = safestrcast( retstrs.strptr[ processorInfo.ProcessorManufacturerStrIndex ] );
        if ( processorInfo.ProcessorVersionStrIndex < 0xFF )
            processorInfo.ProcessorVersionString       = safestrcast( retstrs.strptr[ processorInfo.ProcessorVersionStrIndex ] );
        if ( processorInfo.SerialNumberStrIndex < 0xFF )
            processorInfo.SerialNumberString           = safestrcast( retstrs.strptr[ processorInfo.SerialNumberStrIndex ] );
        if ( processorInfo.AssetTagStrIndex < 0xFF )
            processorInfo.AssetTagString               = safestrcast( retstrs.strptr[ processorInfo.AssetTagStrIndex ] );
        if ( processorInfo.PartNumberStrIndex < 0xFF )
            processorInfo.PartNumberString             = safestrcast( retstrs.strptr[ processorInfo.PartNumberStrIndex ] );

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }

}

void Win32BIOSinfomation::remapMemCtrlrInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    PBYTE startptr = pbuff;
    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        unsigned fixedlen = refheader->Length;
        float    smbver   = SMB->MajorVersion + (float)( SMB->MinorVersion / 10.f );

        if ( smbver >= 2.0f )
        {
            memcpy( &memCtrlrInfo, pbuff, 15 );
            pbuff += 15;

            if ( memCtrlrInfo.AMslotSize > 0 )
            {
                for ( int cnt=0; cnt<memCtrlrInfo.AMslotSize; cnt++)
                {
                    int cpsz = 2 * memCtrlrInfo.AMslotSize;
                    memcpy( memCtrlrInfo.AMSlot, pbuff, cpsz );
                    pbuff += cpsz;
                }
            }
        }

        if ( smbver >= 2.1f )
        {
            memCtrlrInfo.ECCcapbilities = *pbuff++;
        }

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void Win32BIOSinfomation::remapMemModuleInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    PBYTE startptr = pbuff;
    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        int fixedlen = refheader->Length;

        memcpy( &memModuleInfo, pbuff, fixedlen );
        pbuff += fixedlen;

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void Win32BIOSinfomation::remapCacheInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    PBYTE startptr = pbuff;
    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        unsigned fixedlen = refheader->Length;
        unsigned maxsidx = 1;

        memcpy( &cacheInfo[cacheCount], pbuff, fixedlen );
        pbuff += fixedlen;

        remapstringptrs retstrs;

        unsigned idx = cacheInfo[cacheCount].SocketDesignStrIndex;

        if ( idx < 0xFF )
            maxsidx = _MAX( maxsidx, idx );

        getPStringPtrs( maxsidx, retstrs );

        if ( idx < 0xFF )
            cacheInfo[cacheCount].SocketDesignString = safestrcast( retstrs.strptr[ idx ] );

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
        cacheCount++;
    }
}

void Win32BIOSinfomation::remapSysSlotsInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        int fixedlen = refheader->Length;
        unsigned maxsidx = 1;

        if ( sysSlotCount >= MAX_DEVICESLOT_COUNT )
        {
            pbuff += fixedlen + 1;
            return;
        }

        memcpy( &sysSlotsInfo[sysSlotCount], pbuff, fixedlen );
        pbuff += fixedlen;

        remapstringptrs retstrs;

        if ( sysSlotsInfo[sysSlotCount].SlotDesignStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, sysSlotsInfo[sysSlotCount].SlotDesignStrIndex );

        getPStringPtrs( maxsidx, retstrs );

        if ( sysSlotsInfo[sysSlotCount].SlotDesignStrIndex < 0xFF )
            sysSlotsInfo[sysSlotCount].SlotDesignString = \
                safestrcast( retstrs.strptr[ sysSlotsInfo[sysSlotCount].SlotDesignStrIndex ] );

        sysSlotCount++;

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void  Win32BIOSinfomation::remapPortConnectorInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        int fixedlen = refheader->Length;
        unsigned maxsidx = 2;

        if ( portConCount >= MAX_PORT_COUNT )
        {
            pbuff += fixedlen +1;
            return;
        }

        memcpy( &portConInfo[portConCount], pbuff, fixedlen );
        pbuff += fixedlen;

        remapstringptrs retstrs;

        if ( portConInfo[portConCount].InternalRefDesignStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, portConInfo[portConCount].InternalRefDesignStrIndex );
        if ( portConInfo[portConCount].ExternalRefDesignStrIndex < 0xFF )
            maxsidx = _MAX( maxsidx, portConInfo[portConCount].ExternalRefDesignStrIndex );

        getPStringPtrs( maxsidx, retstrs );

        if ( portConInfo[portConCount].InternalRefDesignStrIndex < 0xFF )
            portConInfo[portConCount].InternalRefDesignString = \
                safestrcast( retstrs.strptr[ portConInfo[portConCount].InternalRefDesignStrIndex ] );
        if ( portConInfo[portConCount].ExternalRefDesignStrIndex < 0xFF )
            portConInfo[portConCount].ExternalRefDesignString = \
                safestrcast( retstrs.strptr[ portConInfo[portConCount].ExternalRefDesignStrIndex ] );

        portConCount++;

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void Win32BIOSinfomation::remapSystemReset()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        //int fixedlen = refheader->Length;
        int fixedlen = sizeof( SysResetInfo );

        memcpy( &sysResetInfo, pbuff, fixedlen );
        pbuff += fixedlen;

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void  Win32BIOSinfomation::remapPhysMemArrayInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        unsigned cpsz   = sizeof( SMBIOSHEADER );
        float    smbver = SMB->MajorVersion + (float)( SMB->MinorVersion / 10.f );

        if ( smbver >= 2.1f )
        {
            memcpy( &physMemArrayInfo, pbuff, cpsz );
            pbuff += cpsz;

            physMemArrayInfo.Location = *pbuff++;
            physMemArrayInfo.Use      = *pbuff++;
            physMemArrayInfo.MemEC    = *pbuff++;

            memcpy( &physMemArrayInfo.MaxCapa, pbuff, 4 );
            pbuff += 4;

            memcpy( &physMemArrayInfo.MemEIH, pbuff, 2 );
            pbuff += 2;

            memcpy( &physMemArrayInfo.NumMemDevs, pbuff, 2 );
            pbuff += 2;
        }

        if ( smbver >= 2.7f )
        {
            memcpy( &physMemArrayInfo.ExtendedMaxCapa, pbuff, 8 );
            pbuff += 8;
        }

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void Win32BIOSinfomation::remapMemoryDeviceInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        int fixedlen = refheader->Length;
        unsigned maxsidx = 2;

        if ( memDeviceCount >= MAX_MEMORY_COUNT )
        {
            pbuff += fixedlen;
            skipStringInfo();
            pbuff++;
            return;
        }

        memcpy( &memDeviceInfo[memDeviceCount], pbuff, fixedlen );
        pbuff += fixedlen;

        remapstringptrs retstrs;

        if ( memDeviceInfo[memDeviceCount].DeviceLocatorIndex < 0xFF )
            maxsidx = _MAX( maxsidx, memDeviceInfo[memDeviceCount].DeviceLocatorIndex );

        if ( memDeviceInfo[memDeviceCount].BankLocatorIndex < 0xFF )
            maxsidx = _MAX( maxsidx, memDeviceInfo[memDeviceCount].BankLocatorIndex );

        if ( memDeviceInfo[memDeviceCount].ManufacturerIndex < 0xFF )
            maxsidx = _MAX( maxsidx, memDeviceInfo[memDeviceCount].ManufacturerIndex );

        if ( memDeviceInfo[memDeviceCount].SerialNumberIndex < 0xFF )
            maxsidx = _MAX( maxsidx, memDeviceInfo[memDeviceCount].SerialNumberIndex );

        if ( memDeviceInfo[memDeviceCount].AssetTagIndex < 0xFF )
            maxsidx = _MAX( maxsidx, memDeviceInfo[memDeviceCount].AssetTagIndex );

        if ( memDeviceInfo[memDeviceCount].PartNumberIndex < 0xFF )
            maxsidx = _MAX( maxsidx, memDeviceInfo[memDeviceCount].PartNumberIndex );

        getPStringPtrs( maxsidx, retstrs );

        if ( memDeviceInfo[memDeviceCount].DeviceLocatorIndex < 0xFF )
            memDeviceInfo[memDeviceCount].DeviceLocator = \
                safestrcast( retstrs.strptr[ memDeviceInfo[memDeviceCount].DeviceLocatorIndex ] );

        if ( memDeviceInfo[memDeviceCount].BankLocatorIndex < 0xFF )
            memDeviceInfo[memDeviceCount].BankLocator = \
                safestrcast( retstrs.strptr[ memDeviceInfo[memDeviceCount].BankLocatorIndex ] );

        if ( memDeviceInfo[memDeviceCount].ManufacturerIndex < 0xFF )
            memDeviceInfo[memDeviceCount].Manufacturer = \
                safestrcast( retstrs.strptr[ memDeviceInfo[memDeviceCount].ManufacturerIndex ] );

        if ( memDeviceInfo[memDeviceCount].SerialNumberIndex < 0xFF )
            memDeviceInfo[memDeviceCount].SerialNumber = \
                safestrcast( retstrs.strptr[ memDeviceInfo[memDeviceCount].SerialNumberIndex ] );

        if ( memDeviceInfo[memDeviceCount].AssetTagIndex < 0xFF )
            memDeviceInfo[memDeviceCount].AssetTag = \
                safestrcast( retstrs.strptr[ memDeviceInfo[memDeviceCount].AssetTagIndex ] );

        if ( memDeviceInfo[memDeviceCount].PartNumberIndex < 0xFF )
            memDeviceInfo[memDeviceCount].PartNumber = \
                safestrcast( retstrs.strptr[ memDeviceInfo[memDeviceCount].PartNumberIndex ] );

        memDeviceCount++;

        // Last one byte is CRC, but never used, just NULL.
        pbuff++;
    }
}

void Win32BIOSinfomation::skipUnsupportedInfo()
{
    if ( pbuff == NULL )
        return;

    if ( SMB == NULL )
        return;

    PBYTE startptr = pbuff;
    SMBIOSHEADER* refheader = (SMBIOSHEADER*)pbuff;

    if ( refheader->Length > 0 )
    {
        pbuff += refheader->Length;
    }

    skipStringInfo();
}

void Win32BIOSinfomation::skipStringInfo()
{
    unsigned stopcnt = 0;

    if ( ( pbuff+1 < pbuff_end ) && ( *pbuff != 0x00 ) )
    {
        if ( *(pbuff+1) != 0x00 )
            return;
    }

    while( true )
    {
        if ( pbuff+1 < pbuff_end )
        {
#if 1
            if ( *pbuff == 0x00 )
            {
                stopcnt++;
            }
            else
            {
                if ( stopcnt > 1 )
                {
                    break;
                }

                stopcnt = 0;
            }

            pbuff++;
        }
#else
            pbuff++;

            if ( ( stopcnt == 0 ) && ( *pbuff == 0x00 ) )
            {
                stopcnt++;
            }
            else
            if ( ( stopcnt > 0 ) && ( *pbuff != 0x00 ) )
                break;
        }
        else
            break;
#endif // 1
    }
}

void Win32BIOSinfomation::printinhex( const unsigned char* ref, int refsz )
{
    int  rcnt = 0;
    char humancode[17] = {0};
    int  hcnt = 0;

    for( int cnt=0; cnt<refsz; cnt++ )
    {
        if ( cnt+1 < refsz )
        {
            printf( "%02X ", ref[cnt] );
            if ( ( ( ref[cnt] <= 'Z' ) && ( ref[cnt] >= 'A' ) ) ||
                 ( ( ref[cnt] <= 'z' ) && ( ref[cnt] >= 'a' ) ) ||
                 ( ( ref[cnt] <= '9' ) && ( ref[cnt] >= '0' ) ) )
            {
                humancode[hcnt] = ref[cnt];
            }
            else
            {
                humancode[hcnt] = '.';
            }

            hcnt++;
            rcnt++;

            if ( rcnt > 14 )
            {
                printf( "%s\n", humancode );
                rcnt = 0;
                hcnt = 0;
            }
        }
    }
}

char* Win32BIOSinfomation::safestrcast( const char* p )
{
    if ( p != NULL )
    {
        const char* ref = p;

        while( true )
        {
            if ( ( *ref >=32 ) && ( *ref <= 126 ) )
            {
                ref++;
            }
            else
            {
                size_t refsz = ref - p;
                if ( refsz > 0 )
                {
                    char* newstr = new char[ refsz + 1 ];
                    if ( newstr != NULL )
                    {
                        memset( newstr, 0, refsz +1 );
                        memcpy( newstr, p, refsz );
                        return newstr;
                    }
                }
            }
        }
    }

    return NULL;
}
