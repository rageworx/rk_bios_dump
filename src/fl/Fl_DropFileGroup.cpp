#include "Fl_DropFileGroup.H"

#include <FL/fl_draw.H>
#include <FL/Fl_Pixmap.H>

const char* drag_here_xpm[]=
{
    "83 7 2 1",
    "o c #FFFFFF",
    "  c None",
    "                                                                                   ",
    " oooo  oooo   ooo  oooo    ooooo o o     ooooo   o   o ooooo oooo  ooooo   o o o   ",
    " o   o o   o o   o o   o   o     o o     o       o   o o     o   o o        o o o  ",
    " o   o oooo  o   o oooo    oooo  o o     oooo    ooooo oooo  oooo  oooo      o o o ",
    " o   o o   o o   o o       o     o o     o       o   o o     o   o o        o o o  ",
    " oooo  o   o  ooo  o       o     o ooooo ooooo   o   o ooooo o   o ooooo   o o o   ",
    "                                                                                   ",
};

static Fl_Pixmap staticDropHereXPM( drag_here_xpm );

Fl_DropFileGroup::Fl_DropFileGroup( int X, int Y, int W, int H )
 : Fl_Group( X, Y, W,H ),
   resistduplicatedevent( false ),
   dndeventing( false ),
   dragfilepath( NULL )
{
}

Fl_DropFileGroup::~Fl_DropFileGroup()
{
    if ( dragfilepath != NULL )
    {
    free( dragfilepath );
    }
}

int Fl_DropFileGroup::handle( int event )
{
    int reti = Fl_Group::handle( event );

    if ( resistduplicatedevent == true )
        return reti;

    switch ( event )
    {
        case FL_DND_ENTER:
        case FL_DND_DRAG:
            {
                reti = 1;

                if ( dndeventing == false )
                {
                    dndeventing = true;
                    redraw();
                }
            }
            break;

        case FL_DND_LEAVE:
        case FL_DND_RELEASE:
            {
                reti = 1;

                if ( dndeventing == true )
                {
                    dndeventing = false;
                    draw();
                }
                else
                {
                    redraw();
                }

            }
            break;

        case FL_PASTE:
            {
                resistduplicatedevent = true;
                dndeventing = false;

                if ( callback() != NULL )
                {
                    if ( dragfilepath != NULL )
                    {
                        free( dragfilepath );
                    }

                    if ( Fl::event_text() != NULL )
                    {
                        dragfilepath = strdup( Fl::event_text() );
                    }

                    do_callback();
                }

                draw();

                resistduplicatedevent = false;
            }
            break;
    }

    return reti;
}

void Fl_DropFileGroup::draw()
{
    fl_push_clip( x(), y(), w(), h() );
    Fl_Group::draw();

    if ( dndeventing == true )
    {
        Fl_Color prev_col = fl_color();

        fl_rectf( x(), y(), w(), h(), color() );
        fl_color( labelcolor() );

        staticDropHereXPM.draw( 10, 10, staticDropHereXPM.w() * 2, staticDropHereXPM.h() * 2 );

        fl_color( prev_col );
    }
    else
    {
        if ( parent() != NULL )
        {
            parent()->redraw();
        }
    }

    fl_pop_clip();
}
