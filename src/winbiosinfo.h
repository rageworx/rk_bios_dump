#ifndef __WINBIOSINFO_H__
#define __WINBIOSINFO_H__

#include <windows.h>

/* =============================================================================
** library/source : win32biosinfo for MinGW-W64
** =============================================================================
** (C)Copyright 2016,2017,2020 Raph.Kay.
** -----------------------------------------------------------------------------
**
** Reference : DMTF / DSP0132 ( 2011-01-26 ) version 2.7.1
**             System Management BIOS (SMBIOS) Reference Specification
**
** Updated   : DMTF / DSP0134 ( 2015-02-12 ) version 3.0.0
**
** required base types : 0, 1, 3, 4, 7, 9, 16, 17, 19, 32
**
** Customized issue : Some types not followed standard of DMTF,
**                    Some 'Unknown' members mean this not-standard issue.
**
** Code compatibles : may works in latest version of Visual Suckudio.
**/

#ifdef __GNUC__
    #define BYTEALIGN_1    __attribute__ ( ( aligned( 1 ) ) )
#else
    #define BYTEALIGN_1     __declspec( align( 2 ) ) declarator
#endif // __GNUC__

#ifndef QWORD
    typedef unsigned long long      QWORD;
#endif

class Win32BIOSinfomation
{
    #define RSMB_BUFFER_SIZE        512 * 1024      // 512KB.
    #define MAX_CACHE_COUNT         8
    #define MAX_DEVICESLOT_COUNT    32
    #define MAX_PORT_COUNT          64
    #define MAX_MEMORY_COUNT        64

    public:
        typedef struct _RawSMBIOSData
        {
            BYTE    Used20CallingMethod;
            BYTE    MajorVersion;
            BYTE    MinorVersion;
            BYTE    DMIRevision;
            DWORD   Length;
            BYTE    SMBIOSTableData[];
        } BYTEALIGN_1 RawSMBIOSData;

        typedef struct _SMBIOSHEADER_
        {
            BYTE Type;
            BYTE Length;
            WORD Handle;
        } BYTEALIGN_1 SMBIOSHEADER;

        // SMBIOS type [00] ----------------------------------------------------
        typedef struct _BIOSInfo
        {
            SMBIOSHEADER	Header;
            BYTE    VendorStrIndex;
            BYTE    VersionStrIndex;
            WORD    StartSegment;
            BYTE    ReleaseStrIndex;
            BYTE    ROMsize;
            BYTE    Characteristics[8];
            BYTE    Ext1;
            BYTE    Ext2;
            BYTE    ReleaseMajor;
            BYTE    ReleaseMinor;
            BYTE    EFMajor;
            BYTE    EFMinor;

            PCHAR   VendorString;
            PCHAR   VersionString;
            PCHAR   ReleaseString;
        } BYTEALIGN_1 BIOSInfo;

        // SMBIOS type [01] ----------------------------------------------------
        typedef struct _SystemInfo
        {
            SMBIOSHEADER	Header;
            BYTE    ManufacturerStrIndex;
            BYTE    ProductNameStrIndex;
            BYTE    VersionStrIndex;
            BYTE    SerialNumberStrIndex;
            BYTE    UUID[16];
            BYTE    WakeUpType;
            BYTE    SKUNumberStrIndex;
            BYTE    FamilyStrIndex;

            PCHAR   ManufacturerString;
            PCHAR   ProductNameString;
            PCHAR   VersionString;
            PCHAR   SerialNumberString;
            PCHAR   SKUNumberString;
            PCHAR   FamilyString;
        } BYTEALIGN_1 SystemInfo;

        // SMBIOS type [02] : Baseboard ( or Module ) Information
        typedef struct _BoardInfo
        {
            SMBIOSHEADER	Header;
            BYTE    ManufacturerStrIndex;
            BYTE    ProductNameStrIndex;
            BYTE    VersionStrIndex;
            BYTE    SKUNumberStrIndex;
            BYTE    AssetTagStrIndex;
            BYTE    FeatureFlags;
            BYTE    LocationInChassisStrIndex;
            WORD    ChassisHandle;
            BYTE    BoardType;
            BYTE    NumbersOfCOH;
            WORD*   COH[256];

            PCHAR   ManufacturerString;
            PCHAR   ProductNameString;
            PCHAR   VersionString;
            PCHAR   SKUNumberString;
            PCHAR   AssetTagString;
            PCHAR   LICString;
        } BYTEALIGN_1 BoardInfo;

        // SMBIOS type [03] ----------------------------------------------------
        typedef struct _EnclosureInfo
        {
            SMBIOSHEADER    Header;
            BYTE    ManufacturerStrIndex;
            BYTE    Type;
            BYTE    VersionStrIndex;
            BYTE    SerialNumberStrIndex;
            BYTE    AssetTagStrIndex;
            BYTE    BootupState;
            BYTE    PowerSupplyState;
            BYTE    ThermalState;
            BYTE    SecurityStatus;
            DWORD   OEMdefined;
            BYTE    Height;
            BYTE    NumberOfPowerCords;
            BYTE    ContainedElementCount;
            BYTE    ContainedElementRecordLength;
            BYTE    ContainedElememts[256];         /// above 2.3
            BYTE    SKUStrIndex;                    /// above 2.7
            BYTE    Unknowndatas[2];                /// maybe above 2.7 ??

            PCHAR   ManufacturerString;
            PCHAR   VersionString;
            PCHAR   SerialNumberString;
            PCHAR   AssetTagString;
            PCHAR   SKUString;
        } BYTEALIGN_1 EnclosureInfo;

        // SMBIOS type [04] ----------------------------------------------------
        typedef struct _ProcessorInfo
        {
            SMBIOSHEADER    Header;
            BYTE    SocketDesignationStrIndex;
            BYTE    ProcessorType;
            BYTE    ProcessorFamily;
            BYTE    ProcessorManufacturerStrIndex;
            BYTE    ProcessorID[8];
            BYTE    ProcessorVersionStrIndex;
            BYTE    Voltage;
            WORD    ExternalClock;
            WORD    MaxSpeed;
            WORD    CurrentSpeed;
            BYTE    Status;
            BYTE    ProcessorUpgrade;
            WORD    L1CacheHandle;
            WORD    L2CacheHandle;
            WORD    L3CacheHandle;
            BYTE    SerialNumberStrIndex;
            BYTE    AssetTagStrIndex;
            BYTE    PartNumberStrIndex;
            BYTE    CoreCount;
            BYTE    CoreEnabled;
            BYTE    ThreadCount;
            WORD    ProcessorCharacteristics;
            WORD    ProcessorFamily2;
            BYTE    UnknownBytes[7];    /// Unknown spec...???

            PCHAR   SocketDesignationString;
            PCHAR   ProcessorManufacturerString;
            PCHAR   ProcessorVersionString;
            PCHAR   SerialNumberString;
            PCHAR   AssetTagString;
            PCHAR   PartNumberString;
            PCHAR   UnknownString;
        } BYTEALIGN_1 ProcessorInfo;

        // SMBIOS type [05] ----------------------------------------------------
        typedef struct _MemCtrlrInfo
        {
            SMBIOSHEADER    Header;
            BYTE    EDM;            /// Error Detecting Method
            BYTE    ECC;            /// Error Correcting Capability
            BYTE    SI;             /// Supported Interleave
            BYTE    CI;             /// Current Interleave
            BYTE    MMMsize;        /// Max Memory Module Size.
            WORD    SupportedSpeeds;
            WORD    SupportedMemTypes;
            BYTE    MMV;            /// Memory Module Voltage
            BYTE    AMslotSize;     /// Numbers of Associated Memory Slots
            WORD    AMSlot[256];    /// Associated Memory Slots
            BYTE    ECCcapbilities; /// Enabled Error Correcting Capabilities
        } BYTEALIGN_1 MemCtrlrInfo;

        // SMBIOS type [06] ----------------------------------------------------
        typedef struct _MemModuleInfo   /// SMBIOS type [06]
        {
            SMBIOSHEADER    Header;
            BYTE    SocketDesignStrIndex;
            BYTE    BankConns;
            BYTE    CurrentSpeed;
            WORD    CurrentMemType;
            BYTE    InstalledSize;
            BYTE    EnableSize;
            BYTE    ErrorStatus;

            PCHAR   SocketDesignString;
        } BYTEALIGN_1 MemModuleInfo;

        // SMBIOS type [07] ----------------------------------------------------
        typedef struct _CacheInfo
        {
            SMBIOSHEADER    Header;
            BYTE    SocketDesignStrIndex;
            WORD    CacheConfig;
            WORD    MaxCacheSize;
            WORD    InstalledSize;
            WORD    SupportedType;
            WORD    CurrentType;
            BYTE    CacheSpeed;
            BYTE    ECType;
            BYTE    SysCacheType;
            BYTE    Associativity;

            PCHAR   SocketDesignString;
        } BYTEALIGN_1 CacheInfo;

        // SMBIOS type [08] ----------------------------------------------------
        typedef struct _PortConInfo
        {
            SMBIOSHEADER    Header;
            BYTE            InternalRefDesignStrIndex;
            BYTE            InternalConnectorType;
            BYTE            ExternalRefDesignStrIndex;
            BYTE            ExteranlConnectorType;
            BYTE            PortType;

            PCHAR           InternalRefDesignString;
            PCHAR           ExternalRefDesignString;
        } BYTEALIGN_1 PortConInfo;

        // SMBIOS type [09] ----------------------------------------------------
        typedef struct _SysSlotsInfo
        {
            SMBIOSHEADER    Header;
            BYTE            SlotDesignStrIndex;
            BYTE            SlotType;
            BYTE            SlotDataBUSwidth;
            BYTE            CurrentUsage;
            BYTE            SlotLength;
            WORD            SlotID;
            BYTE            SlotCharactor1;
            BYTE            SlotCharactor2;
            WORD            SegmnentGroupNum;
            BYTE            BUSNum;
            BYTE            DevFuncNum;

            PCHAR           SlotDesignString;
        } BYTEALIGN_1 SysSlotsInfo;

        // SMBIOS type [10] ----------------------------------------------------
        typedef struct _OnBoardDevInfo
        {
            SMBIOSHEADER    Header;
        }OnBoardDevInfo;

        // SMBIOS type [11] ----------------------------------------------------
        typedef struct _OEMStringsInfo
        {
            SMBIOSHEADER    Header;
        }OEMStringsInfo;

        // SMBIOS type [16] ----------------------------------------------------
        typedef struct _PhysMemArrayInfo
        {
            SMBIOSHEADER    Header;
            BYTE            Location;
            BYTE            Use;
            BYTE            MemEC;  /// Memory Error Correction
            DWORD           MaxCapa;
            WORD            MemEIH; /// Memory Error Info. Handle
            WORD            NumMemDevs;
            QWORD           ExtendedMaxCapa;
        }PhysMemArrayInfo;

        // SMBIOS type [17] ----------------------------------------------------
        typedef struct _MemoryDeviceInfo
        {
            SMBIOSHEADER    Header;
            WORD            PhyMemArrayHandle;
            WORD            PhyErrInfoHandle;
            WORD            TotalWidth;
            WORD            DataWidth;
            WORD            Size;
            BYTE            FormFactor;
            BYTE            DeviceSet;
            BYTE            DeviceLocatorIndex;
            BYTE            BankLocatorIndex;
            BYTE            MemoryType;
            WORD            TypeDetail;
            WORD            Speed;
            BYTE            ManufacturerIndex;
            BYTE            SerialNumberIndex;
            BYTE            AssetTagIndex;
            BYTE            PartNumberIndex;
            BYTE            Attributes;
            DWORD           ExtendedSize;
            WORD            ConfgMemClockSpeed;

            PCHAR           DeviceLocator;
            PCHAR           BankLocator;
            PCHAR           Manufacturer;
            PCHAR           SerialNumber;
            PCHAR           AssetTag;
            PCHAR           PartNumber;
        }MemoryDeviceInfo;

        // SMBIOS type [23] ----------------------------------------------------
        typedef struct _SysResetInfo
        {
            SMBIOSHEADER    Header;
            BYTE            Capapilities;
            WORD            ResetCount;
            WORD            ResetLimit;
            WORD            TimerInterval;
            WORD            Timeout;
        }SysResetInfo;

        // SMBIOS type [33] : 64bit memory info
        typedef struct _x64bitMemErrInfo
        {
            SMBIOSHEADER    Header;
            BYTE    ErrType;
            BYTE    ErrGranularity;
            BYTE    ErrOpr;
        } BYTEALIGN_1 x64bitMemErrInfo;

        /*
        ** for future ....

        // SMBIOS type [xx] : ...
        typedef struct _Info
        {
            SMBIOSHEADER    Header;
        }Info;
        */

    public:
        #define RESULT_OK                   0
        #define RESULT_ERROR                -1
        #define RESULT_

    public:
        Win32BIOSinfomation();
        ~Win32BIOSinfomation();

    public:
        void getVersion( char* retStr );
        bool loadfromRSMB();
        bool loadfrombin( const char* fn );
        bool dumptofile( const char* fn );
        bool loadfrombin( const wchar_t* fn );
        bool dumptofile( const wchar_t* fn );

    public:
        int                 LatestResult()                  { return current_result; }
        UINT                RSMBsize()                      { return rsmbBufferSz; }
        RawSMBIOSData*      SMBIOSdata()                    { return SMB; }
        BIOSInfo*           BIOSinfo()                      { return &biosInfo; }
        SystemInfo*         SystemInformation()             { return &systemInfo; }
        BoardInfo*          BoardInformation()              { return &boardInfo; }
        EnclosureInfo*      EnclosureInformation()          { return &enclosureInfo; }
        ProcessorInfo*      ProcessorInformation()          { return &processorInfo; }
        MemCtrlrInfo*       MemoryControllerInformation()   { return &memCtrlrInfo; }
        PortConInfo*        PortConnectorInformation( unsigned index );
        CacheInfo*          CacheInformation( unsigned index );
        SysSlotsInfo*       SystemSlotsInformation( unsigned index );
        MemoryDeviceInfo*   MemoryDeviceInformation( unsigned index );
        SysResetInfo*       SystemResetInformation()        { return &sysResetInfo; }
        PhysMemArrayInfo*   PhysicalMemoryArrayInfo()       { return &physMemArrayInfo; }
        unsigned            PortCount()                     { return portConCount; }
        unsigned            CacheCount()                    { return cacheCount; }
        unsigned            SysSlotCount()                  { return sysSlotCount; }
        unsigned            MemoryDeviceCount()             { return memDeviceCount; }

    private:
        #define MAX_RETURN_STRING_POINTERS  256
        typedef struct _remapstringptrs
        {
            UINT    sizes;
            PCHAR   strptr[MAX_RETURN_STRING_POINTERS];
        }remapstringptrs;

    private:
        PCHAR getPString( PBYTE pbData );
        void  getPStringPtrs( UINT sz, remapstringptrs &rets );
        void  remapBIOSInfo();
        void  remapSystemInfo();
        void  remapBoardInfo();
        void  remapEnclosureInfo();
        void  remapProcessorInfo();
        void  remapMemCtrlrInfo();
        void  remapMemModuleInfo();
        void  remapCacheInfo();
        void  remapSysSlotsInfo();
        void  remapPortConnectorInfo();
        void  remapSystemReset();
        void  remapPhysMemArrayInfo();
        void  remapMemoryDeviceInfo();
        void  skipUnsupportedInfo();
        void  skipStringInfo();

    private:
        void  clearbuffers();
        void  deallocbuffers();
        void  printinhex( const unsigned char* ref, int refsz );
        char* safestrcast( const char* p );

    private:
        int                 current_result;

    protected:
        bool parse();

    protected:
        RawSMBIOSData*      SMB;
        BIOSInfo            biosInfo;
        SystemInfo          systemInfo;
        BoardInfo           boardInfo;
        EnclosureInfo       enclosureInfo;
        ProcessorInfo       processorInfo;
        CacheInfo           cacheInfo[ MAX_CACHE_COUNT ];
        MemCtrlrInfo        memCtrlrInfo;
        MemModuleInfo       memModuleInfo;
        PortConInfo         portConInfo[ MAX_PORT_COUNT ];
        SysSlotsInfo        sysSlotsInfo[ MAX_DEVICESLOT_COUNT ];
        SysResetInfo        sysResetInfo;
        PhysMemArrayInfo    physMemArrayInfo;
        MemoryDeviceInfo    memDeviceInfo[ MAX_MEMORY_COUNT ];

    protected:
        unsigned            cacheCount;
        unsigned            portConCount;
        unsigned            sysSlotCount;
        unsigned            memDeviceCount;
        bool                biosinforeadn;

    protected:
        BYTE                rsmbBuffer[ RSMB_BUFFER_SIZE ];
        UINT                rsmbBufferSz;
        PBYTE               pbuff;
        PBYTE               pbuff_end;
};


#endif /// of __WINBIOSINFO_H__
