# rk_bios_dump

## Introduce
 - A tiny program to dump BIOS and parsing it for human accessable.
 - Requires my own customized FLTK 1.3.4-1 version ( FLTK 1.3.4.1-ts )

## License
 - MIT License.
 - You can use my source code whatever you want.
 - But no warranty, just remember this please.

## Updated news

### 2017-10-17
 - First commit
